NETFILE= 	/net/smb/pbui@fs.nd.edu/www/teaching/cdt.30010.fa16
COMMON= 	scripts/yasb.py templates/base.tmpl $(wildcard static/yaml/*.yaml)
RSYNC_FLAGS= 	-rv --copy-links --progress --exclude="*.swp" --exclude="*.yaml"
YAML=		$(shell ls pages/*.yaml)
HTML= 		${YAML:.yaml=.html}
AFS=		student02.cse.nd.edu:/afs/nd.edu/coursefa.16/cse/cse10101.01/www

%.html:		%.yaml ${COMMON}
	./scripts/yasb.py $< > $@

all:		${HTML}

install:	all
	mkdir -p ${NETFILE}/static
	rsync ${RSYNC_FLAGS} pages/.	${NETFILE}/.
	rsync ${RSYNC_FLAGS} static/	${NETFILE}/static/.

deploy:		all
	rsync ${RSYNC_FLAGS} pages/.	${AFS}/.
	rsync ${RSYNC_FLAGS} static/	${AFS}/static/.

clean:
	rm -f ${HTML}
